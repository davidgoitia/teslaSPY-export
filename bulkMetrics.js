const {
   get,
   metrics
} = require('./lib')
const fs = require('fs')

const batch = 5000

async function download(metric) {
   const ws = fs.createWriteStream(`bulk/${metric}.json`)
   ws.write('[')
   let i=0;
   let total = 0;
   do {
      const data = await get(metric, i, batch)
      total = data.iTotalRecords
      if (i > 0) ws.write(',')
      data.data.forEach((d, i) => {
         if (i > 0) ws.write(',')
         ws.write('\n')
         ws.write(JSON.stringify(d))
      })
      i+= batch
      console.log('metric', metric, `${i}/${total}`)
   } while (i < total)
   ws.write(']')
   ws.close()
}

fs.rmdirSync('bulk', {recursive: true})
fs.mkdirSync('bulk')

Object.keys(metrics).forEach(download)
