require('dotenv').config();
const axios = require('axios');

exports.config = {
  method: 'get',
  headers: { 
    'authority': 'www.teslaspy.com', 
    'sec-ch-ua': '" Not;A Brand";v="99", "Google Chrome";v="91", "Chromium";v="91"', 
    'accept': 'application/json, text/javascript, */*; q=0.01', 
    'x-requested-with': 'XMLHttpRequest', 
    'sec-ch-ua-mobile': '?0', 
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Safari/537.36', 
    'sec-fetch-site': 'same-origin', 
    'sec-fetch-mode': 'cors', 
    'sec-fetch-dest': 'empty',
    'accept-language': 'es,en;q=0.9,fr;q=0.8', 
    'cookie': `PHPSESSID=${process.env.PHPSESSID}`,
  }
};

exports.get = (metric, ...args) => axios({
    ...exports.config,
    url: urlStart(metric, ...args) + exports.metrics[metric]
}).then((response) => {
  const ret = response.data;
  ret.data = ret.aaData.map(d => exports.data[metric](d))
  delete ret.aaData;
  return ret;
});

urlStart=(metric = 'drive', from = 0, length = 5000, echo = 1) => `https://www.teslaspy.com/get.php?data=${metric}MetricsDataTable&from=&to=&sEcho=${echo}&iDisplayStart=${from}&iDisplayLength=${length}`

exports.metrics = {
    drive: '&iColumns=8&sColumns=%2C%2C%2C%2C%2C%2C%2C&mDataProp_0=0&sSearch_0=&bRegex_0=false&bSearchable_0=true&bSortable_0=true&mDataProp_1=1&sSearch_1=&bRegex_1=false&bSearchable_1=true&bSortable_1=true&mDataProp_2=2&sSearch_2=&bRegex_2=false&bSearchable_2=true&bSortable_2=true&mDataProp_3=3&sSearch_3=&bRegex_3=false&bSearchable_3=true&bSortable_3=true&mDataProp_4=4&sSearch_4=&bRegex_4=false&bSearchable_4=true&bSortable_4=true&mDataProp_5=5&sSearch_5=&bRegex_5=false&bSearchable_5=true&bSortable_5=true&mDataProp_6=6&sSearch_6=&bRegex_6=false&bSearchable_6=true&bSortable_6=true&mDataProp_7=7&sSearch_7=&bRegex_7=false&bSearchable_7=true&bSortable_7=true&sSearch=&bRegex=false&iSortCol_0=1&sSortDir_0=desc&iSortingCols=1',
    clima: '&iColumns=9&sColumns=%2C%2C%2C%2C%2C%2C%2C%2C&mDataProp_0=0&sSearch_0=&bRegex_0=false&bSearchable_0=true&bSortable_0=true&mDataProp_1=1&sSearch_1=&bRegex_1=false&bSearchable_1=true&bSortable_1=true&mDataProp_2=2&sSearch_2=&bRegex_2=false&bSearchable_2=true&bSortable_2=true&mDataProp_3=3&sSearch_3=&bRegex_3=false&bSearchable_3=true&bSortable_3=true&mDataProp_4=4&sSearch_4=&bRegex_4=false&bSearchable_4=true&bSortable_4=true&mDataProp_5=5&sSearch_5=&bRegex_5=false&bSearchable_5=true&bSortable_5=true&mDataProp_6=6&sSearch_6=&bRegex_6=false&bSearchable_6=true&bSortable_6=true&mDataProp_7=7&sSearch_7=&bRegex_7=false&bSearchable_7=true&bSortable_7=true&mDataProp_8=8&sSearch_8=&bRegex_8=false&bSearchable_8=true&bSortable_8=true&sSearch=&bRegex=false&iSortCol_0=0&sSortDir_0=desc&iSortingCols=1',
    charge: '&iColumns=15&sColumns=%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C&mDataProp_0=0&sSearch_0=&bRegex_0=false&bSearchable_0=true&bSortable_0=true&mDataProp_1=1&sSearch_1=&bRegex_1=false&bSearchable_1=true&bSortable_1=true&mDataProp_2=2&sSearch_2=&bRegex_2=false&bSearchable_2=true&bSortable_2=true&mDataProp_3=3&sSearch_3=&bRegex_3=false&bSearchable_3=true&bSortable_3=true&mDataProp_4=4&sSearch_4=&bRegex_4=false&bSearchable_4=true&bSortable_4=true&mDataProp_5=5&sSearch_5=&bRegex_5=false&bSearchable_5=true&bSortable_5=true&mDataProp_6=6&sSearch_6=&bRegex_6=false&bSearchable_6=true&bSortable_6=true&mDataProp_7=7&sSearch_7=&bRegex_7=false&bSearchable_7=true&bSortable_7=true&mDataProp_8=8&sSearch_8=&bRegex_8=false&bSearchable_8=true&bSortable_8=true&mDataProp_9=9&sSearch_9=&bRegex_9=false&bSearchable_9=true&bSortable_9=true&mDataProp_10=10&sSearch_10=&bRegex_10=false&bSearchable_10=true&bSortable_10=true&mDataProp_11=11&sSearch_11=&bRegex_11=false&bSearchable_11=true&bSortable_11=true&mDataProp_12=12&sSearch_12=&bRegex_12=false&bSearchable_12=true&bSortable_12=true&mDataProp_13=13&sSearch_13=&bRegex_13=false&bSearchable_13=true&bSortable_13=true&mDataProp_14=14&sSearch_14=&bRegex_14=false&bSearchable_14=true&bSortable_14=true&sSearch=&bRegex=false&iSortCol_0=1&sSortDir_0=desc&iSortingCols=1',
    car: '&iColumns=4&sColumns=%2C%2C%2C&mDataProp_0=0&sSearch_0=&bRegex_0=false&bSearchable_0=true&bSortable_0=true&mDataProp_1=1&sSearch_1=&bRegex_1=false&bSearchable_1=true&bSortable_1=true&mDataProp_2=2&sSearch_2=&bRegex_2=false&bSearchable_2=true&bSortable_2=true&mDataProp_3=3&sSearch_3=&bRegex_3=false&bSearchable_3=true&bSortable_3=true&sSearch=&bRegex=false&iSortCol_0=0&sSortDir_0=desc&iSortingCols=1'
}

idLink=link => link.replace(/^.*>(.*)<\/a>$/, '$1') || null

exports.data = {
  drive: ([id, date, shift, speed, power, latitude, longitude, heading]) => ({
    id: idLink(id),
    date, // '2021-02-01 08:20:29'
    shift, // P,R,N,D
    speed, // Km/h
    power, // Kw
    latitude, longitude,
    heading // ?? angulo
  }),
  clima: ([date, inside, outside, driver, passenger, fanStatus, climateOn, seatHeaterLeft, seatHeaterRight]) => ({
    date, // '2021-02-01 08:20:29'
    inside, // C
    outside, // C
    driver, // driver target temp ºC
    passenger, // passenger target temp ºC
    fanStatus, // 0-10
    climateOn, // 1 true , 0 false
    seatHeaterLeft, // 0-3
    seatHeaterRight // 0-3
  }),
  charge: ([id, date, timeToFullCharge, power, portLatch, current, rate, fastType, state, level, idealRange, estimatedRange, added, idealRangeAdded, estimatedRangeAdded]) => ({
    id: idLink(id),
    date, // '2021-02-01 08:20:29'
    timeToFullCharge, // '00h 45m',
    power, // Kw
    portLatch, // 'Engaged'
    current, // Amperios
    rate, // km/h '16,90'
    fastType, // <invalid> | combo
    state, // Charging | Disconected | Complete
    level, // % bateria
    idealRange, // Km
    estimatedRange, // Km
    added, // Kwh since last query
    idealRangeAdded, // Km
    estimatedRangeAdded, // Km
  }),
  car: ([date, version, locked, odometer]) => ({
    date, // '2021-02-01 08:20:29'
    version, // 2020.36.16 3e9e4e8dd287
    locked, // null | 0 no | 1 true
    odometer, // km 26455.3
  })
}